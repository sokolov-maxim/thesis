% This is a sample LaTeX input file.  (Version of 12 August 2004.)
%
% A '%' character causes TeX to ignore all remaining text on the line,
% and is used for comments like this one.

\documentclass{article}      % Specifies the document class
\usepackage{fontspec}
\usepackage{polyglossia}

\usepackage{pgfplots}
\pgfplotsset{compat=1.9}

\setdefaultlanguage{russian}
\setmainfont[Mapping=tex-text]{CMU Serif}


                             % The preamble begins here.
\title{An Example Document}  % Declares the document's title.
\author{Leslie Lamport}      % Declares the author's name.
\date{January 21, 1994}      % Deleting this command produces today's date.

\newcommand{\ip}[2]{(#1, #2)}
                             % Defines \ip{arg1}{arg2} to mean
                             % (arg1, arg2).

%\newcommand{\ip}[2]{\langle #1 | #2\rangle}
                             % This is an alternative definition of
                             % \ip that is commented out.


\begin{document}             % End of preamble and beginning of text.

\maketitle                   % Produces the title.
111111111111
\include{empty}


\section{New version of Vector QD-algorithm}
\subsection{Introduction}
\subsubsection{Definition of classical quotient-difference
scheme (QD-scheme).}
Let ${s_n}$ is a sequence of elements of an additive (abelian) group. \\
The classical \it difference scheme \rm is two-dimensional array:
\begin{equation}
\begin{array}{ccccccccccc}
d_1^{(0)} \\
 & d_2^{(0)} \\
d_1^{(1)}  &  & d_3^{(0)} \\
 & d_2^{(1)} &  & \cdots \\
d_1^{(2)}  &  & \cdots \\
& \cdots \\
\cdots \\
\end{array}
\end{equation}
where $d_1^{(n)}=s_{n+1}-s_n;d_{m+1}^{(n)}=d_{m}^{(n+1)}$. \\
Due to Rutishauser ~\cite{Rutishauser} \it quotient-difference
scheme \rm (QD-algorithm) is the calculation of QD-table:
\begin{equation}
\begin{array}{ccccccccccc}
q_1^{(0)} \\
& e_1^{(0)} \\
q_1^{(2)}  &  & q_2^{(0)} \\
& e_1^{(2)} &  & \cdots \\
q_1^{(4)}  &  & \cdots \\
& \cdots \\
\cdots \\
\end{array}
\end{equation}
where for $m=1,2,\ldots,n=0,1,2,\ldots$
\begin{eqnarray}
\label{Rhombus1} e_0^{(m)}=0 & &
e_m^{(n)}=[q_m^{(n+1)}-q_m^{(n)}]+e_{m-1}^{(n+1)} \\
\label{Rhombus2} q_1^{(n)}=\displaystyle\frac{s_{n+1}}{s_n} & &
q_{m+1}^{(n)}=\displaystyle\frac{e_{m}^{(n+1)}}{e_{m}^{(n)}}q_m^{(n+1)}
\end{eqnarray}
Each of rules connects four adjacent elements in rhombuslike
configuration of the QD-scheme. That's why equations
(~\ref{Rhombus1})-(~\ref{Rhombus2}) usually referred to as \it
rhombus rules \rm. \\
\subsubsection{Important properties}
In applications the initial data for QD-algorithm is usually the
coefficients of some formal power
series. Here comes an important property of QD-scheme.\\
\it Given $g(z)=s_0+s_1z+s_2z^2+\ldots$  a formal power series,
There exists precisely one continued fraction $C$, corresponding
to $g$ if and only if Hankel determinants
\begin{equation}
H_r^{(\nu)}=\left| \begin{array}{cccccccccccc} s_\nu & s_{\nu+1} & \ldots & s_{\nu+r-1} \\
s_{\nu+1} & s_{\nu+2} & \ldots & s_{\nu+r} \\
\ldots & \ldots & \ldots & \ldots \\
s_{\nu+r-1} & s_{\nu+r} & \ldots & s_{\nu+2r-2} \\
\end{array}\right|
\end{equation}
satisfy $H_r^{(\nu)}\not =0,\nu=0,1\ldots, r=1,2,\ldots$. The
continued fraction $C$ has the following representation in
coefficients QD-scheme
\begin{equation}
C=\frac{s_0|}{|1}-\frac{q_1^{(0)}|}{|1}-\frac{e_1^{(0)}|}{|1}-\frac{q_2^{(0)}|}{|1}-\frac{e_2^{(0)}|}{|1}-\cdots
\end{equation}
\rm \\
Another important analytical property of the QD-scheme is the
following: \\
\it Let $P$ be a meromorphic function with simple pole $z_i$ and
simple zeros $w_i$ that satisfy
\begin{eqnarray}
0<|z_1|<|z_2|<|z_3|<\ldots \nonumber\\
0<|w_1|<|w_2|<|w_3|<\ldots \nonumber
\end{eqnarray}
QD-scheme gives the zeros and poles of a meromorphic function
simultaneously ($m=1,2,\ldots$).
\begin{eqnarray}
\lim\limits_{\nu \rightarrow \infty} q_m^{(\nu)}=z^{-1}_m ,  \\
\lim\limits_{\nu \rightarrow \infty} e_{\nu+m-1}^{(1-n)}=w^{-1}_m
\end{eqnarray}
\rm \\
\subsubsection{Existence and stability}
The question of existence of QD-scheme is can be clarified by the
following relations.
\begin{eqnarray}
q_m^{(\nu)}=\frac{H_m^{(\nu+1)}H_{m-1}^{(\nu)}}{H_m^{(\nu)}H_{m-1}^{(\nu+1)}}
\\
e_m^{(\nu)}=\frac{H_{m+1}^{(\nu)}H_{m-1}^{(\nu+1)}}{H_m^{(\nu)}H_{m}^{(\nu+1)}}
\end{eqnarray}
It's clear that if one of the coefficients $s_n$ is zero, the
first $q$-column fails to exist. The QD-algorithm as just
described is numerically unstable - the result depends overly on
rounding errors.
\subsubsection{Vector QD-algorithm}
In ~\cite{JVanIzeghem1} and ~\cite{JVanIzeghem2} was given the
generalization of classical QD-algorithm for vector case. We
assume that $\{s_n\}$ is a column of $p$ elements
$s_n=[s_n^{(1)},s_n^{(2)},\ldots,s_n^{(p)}]^{T}$. In this cae
QD-table is the following:
\begin{equation}
\label{QD_Table}
\begin{array}{ccccccccccc}
q_{r-1}^{\nu} &&&&  q_{r}^{\nu-1} \\
&& E_{r-1}^{\nu} &&&& E_{r}^{\nu-1}  \\
q_{r-1}^{\nu+1} &&&& q_{r}^{\nu}    \\
&& E_{r-1}^{\nu+1} &&&& E_{r}^{\nu} \\
q_{r-1}^{\nu+2} &&&& q_{r}^{\nu+1}
\end{array}
\end{equation}
where \begin{equation} \label{QD_E}
E_{r}^{\nu}=(e_{ri}^{\nu})_{i=r-p,\ldots,r-1},r\geq{0},\nu\geq{0}
\end{equation}
Classical rhombus rules was enhanced by the third relation
\begin{equation}
\label{QD_Recurse1}
q_{r+1}^{\nu+1}+e_{r,r-1}^{\nu+1}=q_{r+1}^{\nu}+e_{r+1,r}^{\nu}\\
\end{equation}
\begin{equation}
\label{QD_Recurse2}
e_{r,i}^{\nu+1}q_{i+1}^{\nu+1}+e_{r,i-1}^{\nu+1}=e_{r,i}^{\nu}q_{r+1}^{\nu}+e_{r+1,i}^{\nu},i=r-p+1,\ldots,r-1\\
\end{equation}
\begin{equation}
\label{QD_Recurse3}
e_{r,r-p}^{\nu+1}q_{r-p+1}^{\nu+1}=e_{r,r-p}^{\nu}q_{r+1}^{\nu},r\geq{d}
\end{equation}
It's clear that this doesn't add any stability to computation of
vector QD-table. \\
The main purpose of vector QD-algorithm is to solve inverse
moment problem. Let define the following $(p+2)$ diagonal
non-symmetric matrix:
\begin{equation}
\label{Operator_Matrix} \left(\begin{array}{ccccccc}
a_{0,0}&a_{0,1}&0&0&0&0&\cdots\\
a_{1,0}&a_{1,1}&a_{1,2}&0&0&0&\cdots\\
\cdots&\cdots&\cdots&\cdots&\cdots&\cdots&\cdots\\
a_{p,0}&a_{p,1}&a_{p,2}&\cdots&a_{p,p+1}&0&\cdots\\
0&a_{p+1,1}&a_{p+1,2}&\cdots&a_{p+1,p+1}&a_{p+1,p+2}&\cdots\\
\cdots&\cdots&\cdots&\cdots&\cdots&\cdots&\cdots
\end{array}\right)
\end{equation}
with the conditions:
$a_{n,n-p}\not=0,a_{n,n+1}\not=0$ and $a_{i,j}=0,j>i+1,i>j+p$.\\
Vectors of size $p$
\begin{equation}
\label{Moment_Vector}
s_n:=(s_n^{(1)},s_n^{(2)},\ldots,s_n^{(p)}), \mbox{  }
s_n^{(j)}=(A^ne_j,e_0), j=1,2,\ldots,p
\end{equation}
is called \it moment vectors \rm of the operator $A$.\\
Inverse moment problem ~\cite{} is to compute operator matrix
elements from given system of  moments $\{s_n\}$. Elements of
operator matrix are the coefficients of the recurrence relation of
vector orthogonal polynomials, associated to the set of moments.
\begin{eqnarray}
Q_n(z)g_j(z)-P_n^{(j)}(z)=cz^{n_j+1}+\cdots \nonumber\\
Q_{n+1}(z)=(z-a_{n,n})Q_n(z)-a_{n,n-1}Q_{n-1}(z)-\ldots-a_{n,n-p}Q_{n-p}(z)\nonumber
\end{eqnarray}
Polynomials $P^{(j,\nu)}, Q^{\nu}, \nu =0 , \ldots$ are vector
orthogonal polynomials. Let's introduce $p$ linear functionals
\begin{eqnarray}
L_j(z^i)=s_i^{(j)},j=1,2,\ldots,p\nonumber
\end{eqnarray}
Orthogonal conditions for
$\overrightarrow{n}=(n_1,n_2,\ldots,n_p)=(\underbrace{k+1,\ldots,k+1}_{d},\underbrace{k,\ldots,k}_{p-d}),
k\in{\mbox{Z}}_{+},n=pk+d$ are:\\
\\
\begin{equation}
\label{OrthogonalCondition} L_j(Q^{(\nu)}_n(x)x^{k+\nu})=0,\mbox{
}k=0,1,\ldots,n_j-1,j=1,2,\ldots,p
\end{equation}



It was found ~\cite{JVanIzeghem1} the following relations between
elements of operator matrix and elements of QD-table.
\begin{eqnarray}
a_{r,r}=q_{r+1}+e_{r,r-1}\nonumber\\
a_{r,r-1}=e_{r,r-2}+e_{r,r-1}q_{r}\nonumber\\
\cdots\nonumber\\
a_{r,r-p+1}=e_{r,r-p-1}+e_{r,r-p}q_{r-p+2}\nonumber\\
a_{r,r-p}=e_{r,r-p}q_{r-p+1}\nonumber
\end{eqnarray}\\
In case of existence vector QD-algorithm has the same limitations
as in classical case. If initial set of moments $\{s_n\}$ consists
of several zeros - usually QD-algorithm fails to start. \\
For example, case of scarce matrix of operator
\begin{equation}
\label{Scarce} \left(\begin{array}{ccccccc}
0 & 1 &0&0&0&0&\cdots\\
0 & 0 & 1&0&0&0&\cdots\\
\cdots&\cdots&\cdots&\cdots&\cdots&\cdots&\cdots\\
a_{1}&0&0&\cdots&1&0&\cdots\\
0&a_{2}&0&\cdots&0&1&\cdots\\
\cdots&\cdots&\cdots&\cdots&\cdots&\cdots&\cdots
\end{array}\right)
\end{equation}
The corresponding set of moments is full of zeros. So vector
QD-algorithm can't properly start
\subsubsection{Overview of new version}
In this article is given the explanation of a new version of
QD-algorithm. With the help of a new version it's possible now to
solve inverse moment problem for scarce matrix of operator.
Classical rhombus rules are enhanced only by equalities This don't
mean additional computations (and as follows unstable numerical
behaviour) in calculating QD-table.


\subsection{New version of QD-algorithm}
The main difference of two versions is the way we choose
$H_r^\nu$. Let define $H_r^\nu, \nu=pk_1+d_1, \nu+r-1=pk_2+d_2$
as:
\begin{equation}
\label{QD_H} \left|
\begin{array}{ccccccccccc}
s_{k_1}^{(d_1+1)} & s_{k_1+1}^{(d_1+1)} & s_{k_1+2}^{(d_1+1)} & \cdots & s_{k_1+r-1}^{(d_1+1)}\\
s_{k_1}^{(d_1+2)} & s_{k_1+1}^{(d_1+2)} & s_{k_1+2}^{(d_1+2)} & \cdots & s_{k_1+r-1}^{(d_1+2)}\\
\cdots & \cdots & \cdots & \cdots & \cdots\\
s_{k_2}^{(d_2)} & s_{k_2+1}^{(d_2)} & s_{k_2+2}^{(d_2)} & \cdots & s_{k_2+r-1}^{(d_2)} \\
s_{k_2}^{(d_2+1)} & s_{k_2+1}^{(d_2+1)} & s_{k_2+2}^{(d_2+1)} & \cdots & s_{k_2+r-1}^{(d_2+1)} \\
\end{array}
\right|
\end{equation}

From orthogonal conditions we can express monic vector orthogonal
polynomials as:
\begin{equation}
\label{Q_from_H}
\begin{array}{cc}
Q_r^\nu(z)=H_r^\nu(z)/H_r^\nu\\
\left|\begin{array}{ccccc}
s_{k_1}^{(d_1+1)} & s_{k_1+1}^{(d_1+1)} & \cdots & s_{k_1+r}^{(d_1+1)}\\
\cdots & \cdots & \cdots & \cdots\\
s_{k_2}^{(d_2+1)} & s_{k_2+1}^{(d_2+1)} & \cdots & s_{k_2+r}^{(d_2+1)} \\
1               & z               & \cdots & z^r
\end{array}\right|
\times {\left|\begin{array}{cccc}
s_{k_1}^{(d_1+1)} & s_{k_1+1}^{(d_1+1)} & \cdots & s_{k_1+r-1}^{(d_1+1)}\\
\cdots & \cdots & \cdots & \cdots\\
s_{k_2}^{(d_2+1)} & s_{k_2+1}^{(d_2+1)} & \cdots & s_{k_2+r-1}^{(d_2+1)} \\
\end{array}\right|}^{-1}
\end{array}
\end{equation}

Now let prove three theorems to find out relations between
elements of corresponding QD-table.

\bf Theorem 1 \rm \\

For $s \geq 0, n \geq 0$
\begin{equation}
\label{QDQ} Q_{n+1}^\nu =xQ_n^{\nu +p}-q_{n+1}^\nu Q_n^\nu ,
\mbox{ } q_{n+1}^\nu =\frac{H^{\nu +p}_{n+1}H_{n}^\nu
}{H_{n+1}^\nu H_{n}^{\nu +p}}
\end{equation}

\bf Proof \rm \\

Applying Sylvester's identity to $H_{n+1}^\nu $ we have
\begin{eqnarray}
H_{n}^{\nu +p}\cdot H_{n+1}^\nu (x)=xH_{n+1}^\nu  \cdot
H_{n}^{\nu +p}(x) -H^{\nu +p}_{n+1} \cdot H_{n}^\nu  (x) \nonumber
\end{eqnarray}
Dividing by $H_{n}^{\nu +p}\cdot H_{n+1}^\nu $ we get our relation
\begin{eqnarray}
Q_{n+1}^\nu (x)=xQ_{n}^{\nu +p}(x) -\frac{H^{\nu
+p}_{n+1}H_{n}^\nu }{H_{n+1}^\nu H_{n}^{\nu +p}} Q_{n}^\nu  (x)
\nonumber
\end{eqnarray}


\bf Theorem 2 \rm \\

For $\nu  \geq 0, n \geq 0$
\begin{equation}
\label{QDE} Q_{n}^{\nu +p}=Q_{n}^{\nu
}-\sum\limits_{j=1}^{p}{e_{n,n-p+j}^{\nu }Q_{n-1}^{\nu +j}}
\end{equation}

\bf Proof \rm \\

Applying Sylvester's identity to $H_{n}^\nu $ and $H_{n}^{\nu
+1}$ we have
\begin{eqnarray}
H_{n-1}^{\nu +p+1}\cdot H_{n}^\nu (x)=xH_{n}^\nu  \cdot
H_{n-1}^{\nu +p+1}(x)
-H^{\nu +p}_{n} \cdot H_{n-1}^{\nu +1} (x) \nonumber \\
H_{n-1}^{\nu +p+1}\cdot H_{n}^{\nu +1}(x)=xH_{n}^{\nu +1} \cdot
H_{n-1}^{\nu +p+1}(x) -H^{\nu +p+1}_{n} \cdot H_{n-1}^{\nu +1} (x)
\nonumber
\end{eqnarray}
Dividing by corresponding relations we get
\begin{eqnarray}
Q_{n}^\nu (x)=xQ_{n-1}^{\nu +p+1}(x) -\frac{H^{\nu
+p}_{n}H_{n-1}^{\nu +1}}{H_{n}^\nu H_{n-1}^{\nu
+p+1}}Q_{n-1}^{\nu +1} (x) \nonumber \\
Q_{n}^{\nu +1}(x)=xQ_{n-1}^{\nu +p+1}(x) -\frac{H^{\nu
+p+1}_{n}H_{n-1}^{\nu +1}}{H_{n}^{\nu +1}H_{n-1}^{\nu
+p+1}}Q_{n-1}^{\nu +1} (x) \nonumber
\end{eqnarray}
Combining this relations we get
\begin{equation}
Q_{n}^{\nu +1}(x)=Q_{n}^\nu (x)-e_{n,n-p+1}^{\nu }Q_{n-1}^{\nu +1}
(x), \mbox{ }e_{n,n-p+1}^{\nu }=\frac{H^{\nu
+p+1}_{n}H_{n-1}^{\nu +1}}{H_{n}^{\nu +1}H_{n-1}^{\nu +p+1}}-
\frac{H^{\nu +p}_{n}H_{n-1}^{\nu +1}}{H_{n}^\nu H_{n-1}^{\nu
+p+1}}
\end{equation}
Then we compare pairs $(Q_n^{\nu +1},Q_n^{\nu +2}),\ldots
,(Q_n^{\nu +p-1},Q_n^{\nu +p})$
\begin{eqnarray}
Q_{n}^{\nu +1}(x)=Q_{n}^\nu (x)-e_{n,n-p+1}^{\nu }Q_{n-1}^{\nu +1}
(x)
\nonumber \\
Q_{n}^{\nu +2}(x)=Q_{n}^{\nu +1}(x)-e_{n,n-p+2}^{\nu
}Q_{n-1}^{\nu +2} (x)
\nonumber \\ \cdots \nonumber \\
Q_{n}^{\nu +p-1}(x)=Q_{n}^{\nu +p-2}(x)-e_{n,n-1}^{\nu
}Q_{n-1}^{\nu +p-1} (x)
\nonumber \\
Q_{n}^{\nu +p}(x)=Q_{n}^{\nu +p-1}(x)-e_{n,n}^{\nu }Q_{n-1}^{\nu
+p} (x) \nonumber
\end{eqnarray}
From this we can the result of theorem. \\

\bf Theorem 3 \rm \\
\begin{equation}
\label{QD1}
q_{n+1}^{\nu+1}+e_{n,n}^{\nu+1}=q_{n+1}^{\nu}+e_{n+1,n-p}^\nu  \\
\end{equation}
\begin{equation}
\label{QD2} q_{n}^{\nu+1}e_{n,n}^{\nu+1}=q_{n+1}^\nu
e_{n,n-p+1}^\nu
\end{equation}
\begin{equation}
\label{QD3}
 e_{n,n-j+1}^{\nu}=e_{n,n-j}^{\nu+1},\mbox{
}j=p-1,\ldots,1
\end{equation}
\bf Proof \rm \\
Let's begin from last assertion (~\ref{QD3}). Using the proof of
Theorem 2 we can redefine:
\begin{eqnarray}
Q_{n}^{\nu+2}(x)=Q_{n}^{\nu+1}(x)-e_{n,n-p+1}^{\nu+1}Q_{n-1}^{\nu+2}(x)\nonumber\\
\cdots \nonumber \\
Q_{n}^{\nu +p-1}(x)=Q_{n}^{\nu
+p-2}(x)-e_{n,n-2}^{\nu+1}Q_{n-1}^{\nu +p-1} (x)
\nonumber\\
Q_{n}^{\nu +p}(x)=Q_{n}^{\nu +p-1}(x)-e_{n,n-1}^{\nu+1
}Q_{n-1}^{\nu +p} (x) \nonumber
\end{eqnarray}
Then from (~\ref{QDQ}) and (~\ref{QDE})we have
\begin{eqnarray}
Q_{n+1}^\nu =xQ_n^{\nu +p}-q_{n+1}^\nu Q_n^\nu  \nonumber \\
Q_{n+1}^\nu =x\left(Q_{n}^{\nu
}-\sum\limits_{j=1}^{p}{e_{n,n-p+j}^{\nu }Q_{n-1}^{\nu
+j}}\right)-q_{n+1}^\nu Q_n^\nu  \nonumber
\end{eqnarray}
Let's define
\begin{equation}
\left\{
\begin{array}{llllllll}
u_{n,n+1}^\nu = q_{n+1}^\nu \\
u_{n,n-j}^\nu = e_{n,n-j}^{\nu}, j=0,\ldots,p-1
\end{array}
\right.
\end{equation}
\\Using the proof of
Theorem 2 we can write
\begin{eqnarray}
Q_{n+1}^\nu =\left(x-\sum\limits_{i_1=-1}^{p-1}{u_{n,n-i_1}^{\nu
}} \right)Q_n^\nu - \sum\limits_{i_1=0}^{p-1}{u_{n,n-i_1}^{\nu }}
\sum\limits_{i_2=0}^{i_1}{u_{n-1,n-i_2}^{\nu }} Q_{n-1}^{\nu }- \nonumber \\
-\sum\limits_{i_1=1}^{p-1}{u_{n,n-i_1}^{\nu }}
\sum\limits_{i_2=1}^{i_1}{u_{n-1,n-i_2}^{\nu }}
\sum\limits_{i_3=1}^{i_2}{u_{n-2,n-i_3}^{\nu }} Q_{n-2}^{\nu
}-\ldots
\nonumber \\
-\sum\limits_{i_1=p-1}^{p-1}{u_{n,n-i_1}^{\nu }}
\sum\limits_{i_2=p-1}^{i_1}{u_{n-1,n-i_2}^{\nu }}\ldots
\sum\limits_{i_{p+1}=p-1}^{i_p}{u_{n-p,n-i_{p+1}}^{\nu }}
Q_{n-p}^{\nu } \nonumber
\end{eqnarray}
\begin{eqnarray}
\label{QDPR1} Q_{n+1}^\nu
=\left(x-\sum\limits_{i_1=-1}^{p-1}{u_{n,n-i_1}^{\nu }}
\right)Q_n^\nu - \sum\limits_{i_1=0}^{p-1}{u_{n,n-i_1}^{\nu }}
\sum\limits_{i_2=0}^{i_1}{u_{n-1,n-i_2}^{\nu }} Q_{n-1}^{\nu }- \nonumber \\
-\sum\limits_{i_1=1}^{p-1}{u_{n,n-i_1}^{\nu }}
\sum\limits_{i_2=1}^{i_1}{u_{n-1,n-i_2}^{\nu }}
\sum\limits_{i_3=1}^{i_2}{u_{n-2,n-i_3}^{\nu }} Q_{n-2}^{\nu
}-\ldots
\nonumber \\
-{u_{n,n-p+1}^{\nu }}{u_{n-1,n-p+1}^{\nu }}\ldots
{u_{n-p,n-p+1}^{\nu }} Q_{n-p}^{\nu }
\end{eqnarray}
Let's define
\begin{equation}
\left\{
\begin{array}{llllllll}
v_{n,n+1}^\nu = e_{n+1,n-p}^\nu \\
v_{n,n}^\nu = q_{n+1}^\nu \\
v_{n,n-j}^\nu = e_{n,n-j}^{\nu}, j=1,\ldots,p-1
\end{array}
\right.
\end{equation}
From this definition and proved assertion it's clear that
$u_{n,n-j}^{\nu}=v_{n,n-j},j=1,\ldots,p-1$
\\ Now starting from other side
\begin{eqnarray}
Q_n^{\nu+1}=Q_{n}^{\nu }-e_{n,n-p+1}^{\nu}Q_{n-1}^{\nu+1} \nonumber \\
Q_n^{\nu+1}=\left(xQ_{n-1}^{\nu+p}-q_{n}^\nu
Q_{n-1}^\nu\right)-e_{n,n-p+1}^{\nu}Q_{n-1}^{\nu+1} \nonumber
\end{eqnarray}
Then from (~\ref{QDQ}) and (~\ref{QDE})we have
\begin{eqnarray}
\label{QDPR2}
 Q_{n+1}^{\nu+1}
=\left(x-\sum\limits_{i_1=-1}^{p-1}{v_{n,n-i_1}^{\nu }}
\right)Q_n^{\nu+1} - \sum\limits_{i_1=0}^{p-1}{v_{n,n-i_1}^{\nu }}
\sum\limits_{i_2=0}^{i_1}{v_{n-1,n-i_2}^{\nu }} Q_{n-1}^{\nu+1}- \nonumber \\
-\sum\limits_{i_1=1}^{p-1}{v_{n,n-i_1}^{\nu }}
\sum\limits_{i_2=1}^{i_1}{v_{n-1,n-i_2}^{\nu }}
\sum\limits_{i_3=1}^{i_2}{v_{n-2,n-i_3}^{\nu }} Q_{n-2}^{\nu+1
}-\ldots
\nonumber \\
-{v_{n,n-p+1}^{\nu }}{v_{n-1,n-p+1}^{\nu }}\ldots
{v_{n-p,n-p+1}^{\nu }} Q_{n-p}^{\nu+1}
\end{eqnarray}
Now we compare coefficients in (~\ref{QDPR1}) and (~\ref{QDPR2})
taking into account (~\ref{QD3}). From this we get our first
assertion (~\ref{QD1})
\begin{eqnarray}
\sum\limits_{i_1=-1}^{p-1}{u_{n,n-i_1}^{\nu+1}}=\sum\limits_{i_1=-1}^{p-1}{v_{n,n-i_1}^{\nu}}
\Rightarrow
u_{n,n+1}^{\nu+1}+u_{n,n}^{\nu+1}=v_{n,n+1}^{\nu}+v_{n,n}^{\nu} \nonumber\\
q_{n+1}^{\nu+1}+e_{n,n}^{\nu+1}=q_{n+1}^{\nu}+e_{n+1,n-p}^{\nu}\nonumber
\end{eqnarray}
At least we have our (~\ref{QD2}) assertion
\begin{eqnarray}
u_{n,n-p+1}^{\nu+1}\ldots
u_{n-p,n-p+1}^{\nu+1}=v_{n,n-p+1}^{\nu}\ldots
v_{n-p,n-p+1}^{\nu}\Rightarrow \nonumber\\
u_{n-p+1,n-p+1}^{\nu+1}u_{n-p,n-p+1}^{\nu+1}=v_{n-p+1,n-p+1}^{\nu}v_{n-p,n-p+1}^{\nu}\nonumber
\\e_{n-p+1,n-p+1}^{\nu+1}q_{n-p+1}^{\nu+1}=q_{n-p+2}^\nu e_{n-p+1,n}^\nu
 & \Rightarrow & e_{n,n}^{\nu+1}q_{n}^{\nu+1}=q_{n+1}^\nu
e_{n,n-p+1}^\nu \nonumber
\end{eqnarray}

The main idea is the same - computation of vector QD-table $(\nu
\geq 0, n>0)$
\begin{equation}
\begin{array} {ccccccccccc}
q_{n-1}^\nu  & & q_{n}^{\nu -1} \\
& E_{n-1}^\nu  & & E_{n}^{\nu -1} \\
q_{n-1}^{\nu +1} & & q_{n}^{\nu } \\
& E_{n-1}^{\nu +1} & & E_{n}^{\nu } \\
q_{n-1}^{\nu +2} & & q_{n}^{\nu +1} \\
\end{array}
\end{equation}
where $E_n^\nu =(e_{n,n-p+1}, \ldots,
e_{n,n-1},e_{n,n})$ \\
From (~\ref{QDQ}) we can get the following initial conditions:
\begin{eqnarray}
E_n^{\nu}=0, q_n^{\nu}=0, \mbox{   } \nu <0, n<1\\  q_1^\nu
=\frac{H^{\nu +p}_{1}}{H_{1}^\nu }=\frac{s_{\nu +p}^{(1)}}{s_\nu
^{(1)}}, q_2^\nu =\frac{H^{\nu +p}_{2}H_{1}^\nu }{H_{2}^\nu
H_{1}^{\nu +p}}
\end{eqnarray}
Let define algorithm procedure from the beginning \\
\textbf{Algorithm procedure}: \\
1. Calculating $q_1^\nu$ and $q_2^\nu$ from initial conditions \\
2. Calculating $e_{1,-p}^\nu, e_{2,1-p}^\nu$ from (~\ref{QD1}) assertion \\
3. Calculating $e_{1,-p+1}^\nu,\ldots,e_{1,1}^\nu$ and
$e_{2,1-p}^\nu,\ldots,e_{2,2}^\nu$ from
(~\ref{QD3}) assertion \\
4. Calculating $q_3$ from (~\ref{QD2}) assertion \\
and so on \\
At least we get filled QD table. We are interested in $\nu=0$
row.\\ From (~\ref{QDPR1}) we get the representation of
coefficients for initial operator matrix.
\begin{eqnarray}
a_{n,n}=\sum\limits_{i_1=-1}^{p-1}{u_{n,n-i_1}^{\nu }} \nonumber\\
a_{n,n-1}=\sum\limits_{i_1=0}^{p-1}{u_{n,n-i_1}^{\nu }}
\sum\limits_{i_2=0}^{i_1}{u_{n-1,n-i_2}^{\nu }} \nonumber\\
\cdots \nonumber\\
a_{n,n-p}={u_{n,n-p+1}^{\nu }}{u_{n-1,n-p+1}^{\nu }}\ldots
{u_{n-p,n-p+1}^{\nu }} \nonumber
\end{eqnarray}


In comparison with previous version it looks more complex, but we
get advantages in case of scarce matrix (~\ref{Scarce}). The
initial data for algorithm must be improved in this case. From
~\cite{KaliaguineAA} Weyl's functions are the following power
series
\begin{eqnarray}
f_1(z) = (R_ze_0,e_0) =
\frac{f_0^{(1)}}{z}+\frac{f_1^{(1)}}{z^2}+\frac{f_2^{(1)}}{z^3}+\frac{f_3^{(1)}}{z^4}+\ldots
\nonumber\\ \cdots \nonumber \\
f_p(z) = (R_ze_{p-1},e_0) =
\frac{f_0^{(p)}}{z}+\frac{f_1^{(p)}}{z^2}+\frac{f_2^{(p)}}{z^3}+\frac{f_3^{(p)}}{z^4}+\ldots\nonumber
\end{eqnarray}
Let's define new power series system
\begin{eqnarray}
S_1(z^{p+1}) = \frac{1}{z^p}f_1(z) \nonumber\\
\cdots \nonumber \\
S_p(z^{p+1}) = \frac{1}{z}f_{p+1}(z) \nonumber
\end{eqnarray}
Let's look at $p+1$ systems of orthogonal polynomials
\begin{eqnarray}
(S_1(z),S_2(z))  & \hat{Q}_{k(p+1)}\nonumber \\
(S_2(z),S_3(z))  & \hat{Q}_{k(p+1)+1} \nonumber\\
\cdots \\
(S_{p-1}(z),S_p(z))  & \hat{Q}_{k(p+1)+p-1}\nonumber\\
(S_{p}(z),zS_1(z))   & \hat{Q}_{k(p+1)+p}\nonumber\\
(zS_{1}(z),zS_2(z))   & \hat{Q}_{k(p+1)+p+1}\nonumber
\end{eqnarray}
From ~\cite{KaliaguineAA} we have
\begin{equation}
\hat{Q}_{n+1}=\epsilon_{n}\hat{Q}_{n}-a_{n-p+1}\hat{Q}_{n-p},
\mbox{ } \epsilon_n=\left\{
\begin{array} {ll}
z, & n = k(p+1)+p \\ 1, & \mbox{otherwise}
\end{array}
\right.
\end{equation}
Let's look at the connection between $\hat{Q}(x)$ and $Q(x)$
\begin{eqnarray}
\hat{Q}_{p+1}(z)=z\hat{Q}_p(z)-a_{1}\hat{Q}_0(z) & \Rightarrow &
Q_1^{(0)}(z)=zQ_0^{(p)}-a_1Q_0^{(0)} \nonumber
\\ \hat{Q}_{p+2}(z)=\hat{Q}_{p+1}(z)-a_2\hat{Q}_1(z) & \Rightarrow & Q_1^{(1)}(z)=Q_1^{(0)}-a_2Q_0^{(1)} \nonumber\\
& \cdots \nonumber \\
\hat{Q}_{2p+2}(z)=\hat{Q}_{2p+1}(z)-a_{p+1}\hat{Q}_{p}(z) & \Rightarrow & Q_1^{(p)}(z)=Q_1^{(p-1)}-a_{p+1}Q_0^{(p-1)} \nonumber\\
\hat{Q}_{2p+3}(z)=z\hat{Q}_{2p+2}(z)-a_{p+2}\hat{Q}_{p+1}(z) &
\Rightarrow & Q_2^{(0)}(z)=zQ_1^{(p)}-a_{p+2}Q_0^{(p)} \nonumber
\end{eqnarray}
So we can write the following relations:
\begin{eqnarray}
a_1=q_1^0 = u_{0,1}\nonumber  \\
a_2=e_{1,-p}^0 = u_{0,0}\nonumber \\
a_3=e_{1,-p+1}^0 = u_{0,-1}\nonumber \\
\cdots \nonumber \\
a_{p+1}=e_{1,1}^0 = u_{0,1-p} \nonumber \\
a_{p+2}=q_2^0 =u_{1,2} \nonumber \\
\cdots \nonumber
\end{eqnarray}
At least for $n=(p+1)k+d$ we have
\begin{equation}
a_n=u_{k,k+2-d}
\end{equation}
So the upper row (diagonal) of defined vector QD-table gives
explicitly elements of scarce matrix. So let us to sum up. \\
1. We can apply new version of QD-algorithm in scarce operator
matrix case. \\
2. The number of arithmetic operations  decreased. \\



\begin{tikzpicture}
\begin{axis}[ 
    view={110}{10}, 
    colormap/greenyellow,
    colorbar 
]
\addplot3[surf] {-sin(x^2 + y^2)};
\end{axis}
\end{tikzpicture}


2222222222222222222222
\end{document}               % End of document.